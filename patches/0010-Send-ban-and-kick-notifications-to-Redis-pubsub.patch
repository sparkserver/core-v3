From b727f09c657841a997cfd556affd52ad7176bd1a Mon Sep 17 00:00:00 2001
From: redbluescreen <redbluescreen@nextdata.eu>
Date: Tue, 1 Sep 2020 19:56:51 +0300
Subject: [PATCH 10/19] Send ban and kick notifications to Redis pubsub

---
 .../java/com/soapboxrace/core/bo/AdminBO.java | 28 +++++++++++++++----
 .../java/com/soapboxrace/core/bo/RedisBO.java |  4 +++
 .../vo/notifications/BanNotification.java     | 28 +++++++++++++++++++
 .../vo/notifications/KickNotification.java    | 21 ++++++++++++++
 .../core/vo/notifications/PersonaInfo.java    | 21 ++++++++++++++
 5 files changed, 97 insertions(+), 5 deletions(-)
 create mode 100644 src/main/java/com/soapboxrace/core/vo/notifications/BanNotification.java
 create mode 100644 src/main/java/com/soapboxrace/core/vo/notifications/KickNotification.java
 create mode 100644 src/main/java/com/soapboxrace/core/vo/notifications/PersonaInfo.java

diff --git a/src/main/java/com/soapboxrace/core/bo/AdminBO.java b/src/main/java/com/soapboxrace/core/bo/AdminBO.java
index cbb3c9cf..14c1cbbd 100644
--- a/src/main/java/com/soapboxrace/core/bo/AdminBO.java
+++ b/src/main/java/com/soapboxrace/core/bo/AdminBO.java
@@ -6,12 +6,15 @@
 
 package com.soapboxrace.core.bo;
 
+import com.google.gson.Gson;
 import com.soapboxrace.core.api.util.MiscUtils;
 import com.soapboxrace.core.dao.BanDAO;
 import com.soapboxrace.core.dao.PersonaDAO;
 import com.soapboxrace.core.jpa.BanEntity;
 import com.soapboxrace.core.jpa.PersonaEntity;
 import com.soapboxrace.core.jpa.UserEntity;
+import com.soapboxrace.core.vo.notifications.BanNotification;
+import com.soapboxrace.core.vo.notifications.KickNotification;
 import com.soapboxrace.core.xmpp.OpenFireSoapBoxCli;
 import com.soapboxrace.core.xmpp.XmppChat;
 
@@ -33,6 +36,9 @@ public class AdminBO {
     @EJB
     private OpenFireSoapBoxCli openFireSoapBoxCli;
 
+    @EJB
+    private RedisBO redisBO;
+
     public void sendCommand(Long personaId, Long abuserPersonaId, String command) {
         CommandInfo commandInfo = CommandInfo.parse(command);
         PersonaEntity personaEntity = personaDao.find(abuserPersonaId);
@@ -52,7 +58,7 @@ public class AdminBO {
                 openFireSoapBoxCli.send(XmppChat.createSystemMessage("Banned user!"), personaId);
                 break;
             case KICK:
-                sendKick(userEntity.getId(), personaEntity.getPersonaId());
+                sendKick(personaEntity, personaDao.find(personaId));
                 openFireSoapBoxCli.send(XmppChat.createSystemMessage("Kicked user!"), personaId);
                 break;
             case UNBAN:
@@ -82,12 +88,24 @@ public class AdminBO {
         banEntity.setIp(userEntity.getIpAddress());
         banEntity.setHardwareHash(userEntity.getGameHardwareHash());
         banDAO.insert(banEntity);
-        sendKick(userEntity.getId(), personaEntity.getPersonaId());
+
+        sendRedisNotification("ban", new BanNotification(personaEntity, banEntity));
+
+        sendKick(personaEntity, null);
     }
 
-    private void sendKick(Long userId, Long personaId) {
-        openFireSoapBoxCli.send("<NewsArticleTrans><ExpiryTime><", personaId);
-        tokenSessionBo.deleteByUserId(userId);
+    private void sendKick(PersonaEntity persona, PersonaEntity kickedBy) {
+        openFireSoapBoxCli.send("<NewsArticleTrans><ExpiryTime><", persona.getPersonaId());
+        tokenSessionBo.deleteByUserId(persona.getUser().getId());
+
+        sendRedisNotification("kick", new KickNotification(persona, kickedBy));
+    }
+
+    private void sendRedisNotification(String channel, Object obj) {
+        if (redisBO.isEnabled()) {
+            Gson gson = new Gson();
+            redisBO.getConnection().sync().publish(channel, gson.toJson(obj));
+        }
     }
 
     private static class CommandInfo {
diff --git a/src/main/java/com/soapboxrace/core/bo/RedisBO.java b/src/main/java/com/soapboxrace/core/bo/RedisBO.java
index a5f1c88a..0840aabb 100644
--- a/src/main/java/com/soapboxrace/core/bo/RedisBO.java
+++ b/src/main/java/com/soapboxrace/core/bo/RedisBO.java
@@ -52,6 +52,10 @@ public class RedisBO {
         }
     }
 
+    public boolean isEnabled() {
+        return this.redisClient != null;
+    }
+
     public StatefulRedisPubSubConnection<String, String> createPubSub() {
         if (this.redisClient == null) {
             throw new RuntimeException("Redis is disabled!");
diff --git a/src/main/java/com/soapboxrace/core/vo/notifications/BanNotification.java b/src/main/java/com/soapboxrace/core/vo/notifications/BanNotification.java
new file mode 100644
index 00000000..8aaf192e
--- /dev/null
+++ b/src/main/java/com/soapboxrace/core/vo/notifications/BanNotification.java
@@ -0,0 +1,28 @@
+/*
+ * This file is part of the Soapbox Race World core source code.
+ * If you use any of this code for third-party purposes, please provide attribution.
+ * Copyright (c) 2020.
+ */
+
+package com.soapboxrace.core.vo.notifications;
+
+import com.soapboxrace.core.jpa.BanEntity;
+import com.soapboxrace.core.jpa.PersonaEntity;
+
+import java.time.ZoneId;
+import java.time.format.DateTimeFormatter;
+
+public class BanNotification {
+    private PersonaInfo persona;
+    private PersonaInfo bannedBy;
+
+    private String reason;
+    private String endsAt;
+
+    public BanNotification(PersonaEntity persona, BanEntity ban) {
+        this.persona = new PersonaInfo(persona);
+        this.bannedBy = new PersonaInfo(ban.getBannedBy());
+        this.reason = ban.getReason();
+        this.endsAt = DateTimeFormatter.ISO_INSTANT.format(ban.getEndsAt().atZone(ZoneId.systemDefault()).toInstant());
+    }
+}
diff --git a/src/main/java/com/soapboxrace/core/vo/notifications/KickNotification.java b/src/main/java/com/soapboxrace/core/vo/notifications/KickNotification.java
new file mode 100644
index 00000000..3351da87
--- /dev/null
+++ b/src/main/java/com/soapboxrace/core/vo/notifications/KickNotification.java
@@ -0,0 +1,21 @@
+/*
+ * This file is part of the Soapbox Race World core source code.
+ * If you use any of this code for third-party purposes, please provide attribution.
+ * Copyright (c) 2020.
+ */
+
+package com.soapboxrace.core.vo.notifications;
+
+import com.soapboxrace.core.jpa.PersonaEntity;
+
+public class KickNotification {
+    private PersonaInfo persona;
+    private PersonaInfo kickedBy;
+
+    public KickNotification(PersonaEntity persona, PersonaEntity kickedBy) {
+        this.persona = new PersonaInfo(persona);
+        if (kickedBy != null) {
+            this.kickedBy = new PersonaInfo(kickedBy);
+        }
+    }
+}
diff --git a/src/main/java/com/soapboxrace/core/vo/notifications/PersonaInfo.java b/src/main/java/com/soapboxrace/core/vo/notifications/PersonaInfo.java
new file mode 100644
index 00000000..f8c07155
--- /dev/null
+++ b/src/main/java/com/soapboxrace/core/vo/notifications/PersonaInfo.java
@@ -0,0 +1,21 @@
+/*
+ * This file is part of the Soapbox Race World core source code.
+ * If you use any of this code for third-party purposes, please provide attribution.
+ * Copyright (c) 2020.
+ */
+
+package com.soapboxrace.core.vo.notifications;
+
+import com.soapboxrace.core.jpa.PersonaEntity;
+
+public class PersonaInfo {
+    private String name;
+    private long id;
+    private long userId;
+
+    PersonaInfo(PersonaEntity persona) {
+        this.name = persona.getName();
+        this.id = persona.getPersonaId();
+        this.userId = persona.getUser().getId();
+    }
+}
-- 
2.28.0.windows.1

